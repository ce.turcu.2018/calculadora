#FUNCION SUMA
def sumar(a, b):

  return a + b


#FUNCION RESTA
def restar(a, b):

  return a - b


#EJEMPLOS DE USO DE LAS FUNCIONES ANTERIORES
print("La suma de 1 y 2 es:", sumar(1, 2))

print("La suma de 3 y 4 es:", sumar(3, 4))

print("La diferencia de 5 y 6 es:", restar(5, 6))

print("La diferencia de 7 y 8 es:", restar(7, 8))
